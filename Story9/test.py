from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
#BELOM SELESAI

# Create your tests here.
class UnitTestLab9 (TestCase):
	def test_apakah_website_running(self):
		c = Client()
		response = c.get('/')
		self.assertEqual(response.status_code, 200)

	def test_apa_website_menggunakan_Auth_html(self):
		c = Client()
		response = c.get('/')
		self.assertTemplateUsed(response, "base_generic.html")

	def test_apa_ada_button(self):
		c = Client()
		response = c.get('/accounts/login/')
		content = response.content.decode('utf8')
		self.assertIn("button", content)

	def test_apa_ada_tulisan_Log_In(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("Log In", content)

	def test_apa_ada_tulisan_nama_dan_password(self):
		c = Client()
		response = c.get('/accounts/login/')
		content = response.content.decode('utf8')
		self.assertIn("Username", content)
		self.assertIn("Password", content)

	def test_apa_ada_tulisan_Sign_Up(self):
		c = Client()
		response = c.get('/accounts/login/')
		content = response.content.decode('utf8')
		self.assertIn("Sign Up", content)

	def test_apa_ada_tulisan_Sign_In(self):
		c = Client()
		response = c.get('/accounts/signup/')
		content = response.content.decode('utf8')
		self.assertIn("Username", content)
		self.assertIn("Password", content)
		self.assertIn("Sign Up", content)

class functionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(functionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(functionalTest, self).tearDown()

	def test_form_login(self):
		selenium = self.selenium
		selenium.get('http://localhost:8000/accounts/login/')
		time.sleep(3)
		username = selenium.find_element_by_id('id_username')
		kunci = selenium.find_element_by_id('id_password')
		time.sleep(3)

		username.send_keys('Anthony')
		time.sleep(3)
		kunci.send_keys('tonytony')

		enter = selenium.find_element_by_id('login-btn')
		time.sleep(3)
		kunci.send_keys(Keys.RETURN)